using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GuiManager : MonoBehaviour
{

    static public GuiManager _instance;
    private void Start()
    {
        Eventmanager._instance.GUnTimeOver.AddListener(Gunindicator_lowalpha);
        Eventmanager._instance.GUnTimeOver.AddListener(resetgunfilled);
        Eventmanager._instance.Ongunpicked.AddListener(gunINdicator_fullAlpha);
    }
    public UIitems Ui_items;
    private void Awake()
    {
        _instance = this;
    }
    public void restart()
    {
        SceneManager.LoadSceneAsync(0);
    }
    #region Gun Indicator
    public void Gunindicator_lowalpha()
    {
        Ui_items.indicators.gunFillbg.color = new Color(0, 0, 0, 0.4f);
        Ui_items.indicators.gunimg.color = new Color(1, 1, 1, 0.4f);
    }
    public void gunINdicator_fullAlpha()
    {
        Ui_items.indicators.gunFillbg.color = new Color(0, 0, 0, 1f);
        Ui_items.indicators.gunimg.color = new Color(1, 1, 1, 1f);
    }
    void resetgunfilled()
    {
        Ui_items.indicators.gunFillbg.fillAmount = 1;
    }

    #endregion
    [System.Serializable]
    public class UIitems
    {
        public indicators indicators;
    }

    [System.Serializable]
    public class indicators
    {
        public UnityEngine.UI.Image gunFillbg, gunimg;
    }

}
