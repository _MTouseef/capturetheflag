using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rADARTRIGGER : MonoBehaviour
{

    public enum enemy { player, cpu };
    public enemy Enemy;
    string tag = "";
    [SerializeField] GameObject thisobj;
    public SpriteRenderer radarimg;

    Color greencol, redcol;
    private void Start()
    {
        if (Enemy == enemy.player)
            tag = "player";
        else if (Enemy == enemy.cpu)
            tag = "enemy";
        greencol = new Color(Color.green.r, Color.green.g, Color.green.b, 0.7f);
        redcol = new Color(Color.red.r, Color.red.g, Color.red.b, 0.7f);
        radarimg = transform.parent.GetComponent<SpriteRenderer>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == tag)
        {
            green();

            if (Enemy == enemy.player)
            {
                thisobj.GetComponent<Enemy>().canshoot = true;
            }
            else if (Enemy == enemy.cpu)
            {
                thisobj.GetComponent<Player>().canshoot = true;
            }

        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == tag)
        {
            red();

            if (Enemy == enemy.player)
            {
                thisobj.GetComponent<Enemy>().canshoot = false;
            }
            else if (Enemy == enemy.cpu)
                thisobj.GetComponent<Player>().canshoot = false;

        }
    }
    private void OnDisable()
    {

        if (Enemy == enemy.player)
        {
            if (thisobj.GetComponent<Enemy>()) ;
            thisobj.GetComponent<Enemy>().canshoot = false;
        }
        else if (Enemy == enemy.cpu)
            if (thisobj.GetComponent<Player>())
                thisobj.GetComponent<Player>().canshoot = false;

    }


    public void green()
    {
        LeanTween.value(gameObject, redcol, greencol, 0.15f).setOnUpdate(delegate (Color col)
            {
                radarimg.color = col;
            });
    }

    public void red()
    {
        LeanTween.value(gameObject, greencol, redcol, 0.15f).setOnUpdate(delegate (Color col)
              {
                  radarimg.color = col;
              });

    }

}


