﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    static public Gamemanager _instance;
    private void Awake()
    {
        _instance = this;
    }
    public GameObject Gun;
    public GameObject player;
    public bool GameOver;
    public Transform keymoveingpoint;
    private void Start()
    {
        player = GameObject.FindObjectOfType<Player>().gameObject;
        Eventmanager._instance.GameOver.AddListener(delegate () { GameOver = true; });
    }

}
