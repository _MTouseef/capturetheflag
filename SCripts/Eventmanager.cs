using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Eventmanager : MonoBehaviour
{
    static public Eventmanager _instance;
    private void Awake()
    {
        _instance = this;
    }

    public UnityEvent Onshoot;
    public UnityEvent OnDeductHealth;
    public UnityEvent GUnTimeOver;
    public UnityEvent Ongunpicked;
    public UnityEvent GameOver;
}
