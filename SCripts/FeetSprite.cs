﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetSprite : MonoBehaviour
{
    // Start is called before the first frame update

    void Start()
    {
        SpriteRenderer material = transform.GetComponent<SpriteRenderer>();
        Color lastcolor = material.color;
        Color red = new Color(0, 0, 0, 0f);
        LeanTween.value(gameObject, lastcolor, red, 1.4f).setOnUpdate(delegate (Color col)
           {
               material.color = col;


           }).setEaseSpring().setOnComplete(delegate ()
           {
               Destroy(gameObject);
           });
    }


   

}
