using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leveldoor : MonoBehaviour
{
    public bool x, z;
    public float movement, Time;
    public GameObject leftdoor, rightdoor;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            if (other.GetComponent<Player>().haskey)
            {
                if (x)
                {
                    leftdoor.LeanMoveX(leftdoor.transform.position.x + movement, Time);
                    rightdoor.LeanMoveX(rightdoor.transform.position.x - movement, Time);
                }
                else if (z)
                {
                    leftdoor.LeanMoveZ(leftdoor.transform.position.z + movement, Time);
                    rightdoor.LeanMoveZ(rightdoor.transform.position.z - movement, Time);
                }
            }
        }
    }
}
