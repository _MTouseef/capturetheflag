
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public Joystick joystick;
    Rigidbody rigidbody;
    public float speed;
    public GameObject Guninhand;
    public Animator animator;
    public bool withpistol;
    public GameObject radar;
    public bool canshoot;
    public bool haskey;
    public GameObject Guntimer;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }
    public enum State { idle, running, pistol_idle, pistolrunning }
    public State playerstate;
    Vector3 moveposition;
    [HideInInspector] public Quaternion rotatation;
    private void Update()
    {

        if (joystick.Direction.magnitude != 0)
        {
            moveposition = (transform.position + new Vector3(joystick.Direction.y * -1, 0, joystick.Direction.x) * Time.deltaTime * speed);
            rotatation = Quaternion.LookRotation(new Vector3(joystick.Direction.y * -1, 0, joystick.Direction.x));
            transform.rotation = rotatation;
            if (!withpistol)
            {
                run();
            }
            else if (withpistol)
            {
                pistolrunning();
            }
        }
        else if (joystick.Direction.magnitude == 0)
        {
            if (withpistol)
                pistol_idle();
            else if (!withpistol)
            {
                idle();
            }
        }


    }
    void run()
    {
        if (playerstate != State.running)
        {
            animator.SetTrigger("running");
            playerstate = State.running;
        }
    }
    void idle()
    {
        if (playerstate != State.idle)
        {
            animator.SetTrigger("idle");
            playerstate = State.idle;
        }
    }
    void pistolrunning()
    {
        if (playerstate != State.pistolrunning)
        {
            animator.SetTrigger("pistolrun");
            playerstate = State.pistolrunning;
        }
    }
    void pistol_idle()
    {
        if (playerstate != State.pistol_idle)
        {
            animator.SetTrigger("pistolidle");
            playerstate = State.pistol_idle;
        }
    }

    private void FixedUpdate()
    {
        if (joystick.Direction.magnitude > 0.5f)
        {
            Vector3 desiredPosition = moveposition;
            Vector3 currentPosition = transform.position;
            Vector3 direction = desiredPosition - currentPosition;
            Ray ray = new Ray(currentPosition, direction);
            RaycastHit hit;
            if (!Physics.Raycast(ray, out hit, direction.magnitude))
                rigidbody.MovePosition(desiredPosition);
            else
                rigidbody.MovePosition(hit.point);

        }

    }
}
