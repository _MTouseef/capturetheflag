using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMovements : MonoBehaviour
{

    [SerializeField] bool x, z;
    public float Movement_value;
    Vector3 startposition;
    public float movement_Time;
    bool changed;
    void Start()
    {
        startposition = transform.position;
        movetoValue();
    }
    void movetoValue()
    {
        if (x)
            gameObject.LeanMoveX(transform.position.y + Movement_value, movement_Time).setOnComplete(toStartposX);
        else if (z)
            gameObject.LeanMoveZ(transform.position.z + Movement_value, movement_Time).setOnComplete(toStartposZ);


    }

    void toStartposX()
    {
        if (!changed)
        {
            Movement_value *= 2;
            changed = true;
            movement_Time *= 2;
        }
        gameObject.LeanMoveX(transform.position.x - Movement_value, movement_Time).setOnComplete(movetoValue);

    }
    void toStartposZ()
    {
        if (!changed)
        {
            Movement_value *= 2;
            changed = true;
            movement_Time *= 2;
        }
        gameObject.LeanMoveZ(transform.position.z - Movement_value, movement_Time).setOnComplete(movetoValue);

    }

}
