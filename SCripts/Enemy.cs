using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    [HideInInspector] public NavMeshAgent navMesh;
    public GameObject Guninhand;
    public Animator animator;
    public bool withpistol;

    public enum State { idle, running, pistol_idle, pistolrunning }
    public State playerstate;
    public GameObject radar;
    public Transform Player;
    public bool canshoot;
    Transform Gun;
    Gamemanager GM;
    Player playersscript;
    public GameObject GUnTimer;
    public Transform place;
    private void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        Gun = Gamemanager._instance.Gun.transform;
        navMesh.SetDestination(Gamemanager._instance.Gun.transform.position);
        animator = GetComponent<Animator>();
        Player = Gamemanager._instance.player.transform;
        InvokeRepeating("trackplayer", 2, 0.5f);
        GM = Gamemanager._instance;
        playersscript = Player.GetComponent<Player>();
        Eventmanager._instance.GUnTimeOver.AddListener(delegate ()
        {
            navMesh.SetDestination(Gamemanager._instance.Gun.transform.position);
            withpistol = false;
        });
    }


    private void FixedUpdate()
    {
        //Debug.Log(navMesh.velocity);
        animationcontroller();
        if (!GM.GameOver)
        {

            if (!withpistol)
            {
                Vector3 direction = navMesh.desiredVelocity;
                Quaternion rotation = Quaternion.LookRotation(direction);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 10);
            }
            else if (withpistol)
            {
                float Distance = Vector3.Distance(transform.position, Player.position);
                trackplayer();

                if (navMesh.desiredVelocity.magnitude != 0)
                {
                    Vector3 direction = navMesh.desiredVelocity;
                    Quaternion rotation = Quaternion.LookRotation(direction);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 7);
                }
                else
                {
                    Vector3 direction = Player.position - transform.position;
                    Quaternion rotation = Quaternion.LookRotation(direction);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 7);

                }

            }

            if (playersscript.withpistol && !runawaycalled)
            {
                runAway();
            }


        }
    }

    void animationcontroller()
    {
        if (navMesh.velocity.magnitude != 0)
        {
            if (withpistol)
            {
                pistolrunning();
            }
            else if (!withpistol)
            {
                run();
            }

        }
        else if (navMesh.velocity.magnitude == 0)
        {

            {

                if (withpistol)
                {
                    pistol_idle();
                }
                else if (!withpistol)
                {
                    idle();
                }
            }

        }
    }
    public void run()
    {
        if (playerstate != State.running)
        {
            animator.SetTrigger("running");
            playerstate = State.running;
        }
    }
    void idle()
    {
        if (playerstate != State.idle)
        {
            animator.SetTrigger("idle");
            playerstate = State.idle;
        }
    }
    void pistolrunning()
    {
        if (playerstate != State.pistolrunning)
        {
            animator.SetTrigger("pistolrun");
            playerstate = State.pistolrunning;
        }
    }
    void pistol_idle()
    {
        if (playerstate != State.pistol_idle)
        {
            animator.SetTrigger("pistolidle");
            playerstate = State.pistol_idle;
        }
    }

    void trackplayer()
    {
        if (withpistol)
        {
            if (Player)
                navMesh.SetDestination(Player.transform.position);
            if (navMesh.stoppingDistance == 0)
            {
                navMesh.stoppingDistance = 4;
                navMesh.angularSpeed = 0;
            }
        }
    }
    bool runawaycalled;
    public void gundropped()
    {
        if (navMesh.stoppingDistance == 4)
        {
            navMesh.stoppingDistance = 0;
            navMesh.angularSpeed = 0;
            navMesh.speed = 4;
        }
        CancelInvoke("setdest");
        runawaycalled = false;
    }
    public void runAway()
    {

        runawaycalled = true;
        InvokeRepeating("setdest", 0, 1);
    }

    void setdest()
    {
        navMesh.SetDestination((transform.position - Player.position).normalized*10);

    }
}