using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Guntimer : MonoBehaviour
{

    public Transform target;

    Vector3 offset;

    public float time;
    public bool isenemy;
    bool reset;
   public float initialtime;
    public UnityEngine.UI.Image fillImage, gunimg;
    private void Start()
    {
        Eventmanager._instance.GUnTimeOver.AddListener(guntimeOver);
        Eventmanager._instance.Ongunpicked.AddListener(settimeindicator);
        Eventmanager._instance.GameOver.AddListener(delegate ()
        {
            gameObject.LeanCancel();
        });
    }

    void settimeindicator()
    {



        LeanTween.value(gameObject, 1, 0, time).setOnUpdate(delegate (float val)
                   {
                       fillImage.fillAmount = val;
                   }).setOnComplete(delegate ()
                   {
                       Eventmanager._instance.GUnTimeOver?.Invoke();
                   });
    }
    float arc2value = 0;

    void guntimeOver()
    {
        SoundManager._instance.stoptimer();
        if (isenemy && !reset)
        {

            reset = true;
            Enemy enemy = target.GetComponent<Enemy>();
            enemy.Guninhand.SetActive(false);
            enemy.withpistol = false;
            enemy.navMesh.SetDestination(Gamemanager._instance.Gun.transform.position);
            enemy.radar.SetActive(false);
            time = 5;
            Gamemanager._instance.Gun.SetActive(true);
            enemy.gundropped();
            reset = false;
            enemy.run();
            enemy.navMesh.speed = 3.5f;
            gameObject.SetActive(false);

            //  enemy.Guninhand.SetActive(true);
            //     enemy.withpistol = true;
            //     enemy.radar.SetActive(true);
            //     gameObject.SetActive(false);
            //     SoundManager._instance.recoil();
            //     Destroy(gameObject);
        }
        else if (!isenemy && !reset)
        {

            reset = true;
            Player player = target.GetComponent<Player>();
            player.animator.SetTrigger("idle");
            player.playerstate = Player.State.idle;
            player.Guninhand.gameObject.SetActive(false);
            player.withpistol = false;
            player.radar.SetActive(false);
            time = 5;
            Gamemanager._instance.Gun.SetActive(true);
            reset = false;
            gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (target)
            transform.position = target.position + offset;

        if (time > 0)
        {
            time -= Time.deltaTime;
            // percentage = 100 / initialtime;
            // value = (percentage * 360) / 100;

        }


    }
}

