﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Transform player;
    float initial_FOv;
    Camera cm;
    public float target_fov;
    public float time;
    private void Start()
    {
        cm = GetComponent<Camera>();
        initial_FOv = cm.fieldOfView;
        Eventmanager._instance.OnDeductHealth.AddListener(cameraShake);
    }
    public void zoomcamera()
    {


        LeanTween.value(gameObject, initial_FOv, target_fov, time).setOnUpdate(delegate (float value)
            {
                cm.fieldOfView = value;
            }).setEasePunch();

    }
    public void cameraShake()
    {
        LeanTween.cancel(gameObject);
        float shakeAmt = 3 * 0.2f; // the degrees to shake the camera
        float shakePeriodTime = 0.2f; // The period of each shake
        float dropOffTime = 0.24f; // How long it takes the shaking to settle down to nothing

        int x = 0;
        if (x == 0)
        {
            LTDescr shakeTween = LeanTween.rotateAroundLocal(gameObject, Vector3.right, shakeAmt, shakePeriodTime)
            .setEase(LeanTweenType.easeShake) // this is a special ease that is good for shaking
            .setLoopClamp()
            .setRepeat(-1);

            // Slow the camera shake down to zero
            LeanTween.value(gameObject, shakeAmt, 0f, dropOffTime).setOnUpdate(
                (float val) =>
                {
                    shakeTween.setTo(Vector3.right * val);
                }
            ).setEase(LeanTweenType.easeOutQuad);
        }
    }
}
