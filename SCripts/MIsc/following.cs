using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class following : MonoBehaviour
{
    public Transform tofollow;
    Vector3 offset;
    private void Start()
    {
        offset = transform.position - tofollow.position;
        Eventmanager._instance.GameOver.AddListener(delegate ()
        {
            gameObject.SetActive(false);
        });
    }
    private void FixedUpdate()
    {
        if (tofollow)
            transform.position = tofollow.position + offset;
    }
}
