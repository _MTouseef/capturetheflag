using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{

    public GameObject key;

    void Start()
    {
        Invoke("insta", 0.3f);
    }

    void insta()
    {
        Instantiate(key, transform.position + Vector3.up, key.transform.rotation);
    }
}
