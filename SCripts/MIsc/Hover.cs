﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{

    public float amplitude, speed;
    Vector3 target;
    private void Start()
    {
        target = transform.position;

    }

    private void FixedUpdate()
    {

        target += new Vector3(0, amplitude * Mathf.Sin(speed * Time.fixedTime), 0);
        transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * 14f);
        transform.RotateAround(transform.position, transform.up, 1);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {

            Player player = other.GetComponent<Player>();
            if (!player.withpistol)
            {
                player.withpistol = true;
                player.Guntimer.SetActive(true);
                player.Guninhand.SetActive(true);
                player.radar.SetActive(true);
                Eventmanager._instance.Ongunpicked?.Invoke();
                gameObject.SetActive(false);
            }
        }
        else if (other.tag == "enemy")
        {
            Enemy enemy = other.GetComponent<Enemy>();
            if (!enemy.withpistol)
            {
                enemy.GUnTimer.SetActive(true);
                enemy.Guninhand.SetActive(true);
                enemy.withpistol = true;
                enemy.radar.SetActive(true);
                Eventmanager._instance.Ongunpicked?.Invoke();
                gameObject.SetActive(false);
            }
        }

    }

}
