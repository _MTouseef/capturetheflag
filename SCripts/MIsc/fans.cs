using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fans : MonoBehaviour
{
    // Start is called before the first frame update
    public float time;
    public float rotation;
    void Start()
    {
        rotate();
    }
    void rotate()
    {
        LeanTween.rotateAround(gameObject, Vector3.up, rotation, time).setOnComplete(delegate ()
        {
            rotate();
        });

    }

}
