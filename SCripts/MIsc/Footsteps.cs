﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    public GameObject Rightfootstep, Left_footstep;
    Vector3 lastpos;
    public float distance_thresh_Hold;
    enum Feetstate { right, left }
    Feetstate lastfeet;
    SoundManager soundManager;
    private void Start()
    {
        lastpos = transform.position;
        soundManager = SoundManager._instance;
    }
    GameObject feet;
    private void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, lastpos) > distance_thresh_Hold)
        {
            soundManager.footstep();


            if (lastfeet == Feetstate.left)
            {
                feet = Instantiate(Rightfootstep, transform.position, Rightfootstep.transform.rotation);
                lastfeet = Feetstate.right;
            }
            else if (lastfeet == Feetstate.right)
            {
                feet = Instantiate(Left_footstep, transform.position, Rightfootstep.transform.rotation);
                lastfeet = Feetstate.left;
            }
            lastpos = transform.position;
            feet.transform.rotation = Quaternion.LookRotation(transform.forward);
            feet.transform.eulerAngles += new Vector3(90, 0, 0);
        }
    }

}
