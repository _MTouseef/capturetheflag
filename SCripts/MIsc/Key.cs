using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public float amplitude, speed;
    Vector3 target;
    Television television;
    Transform Collecting_position;
    private void Start()
    {
        target = transform.position;
        television = GameObject.FindObjectOfType<Television>();
        Collecting_position = Gamemanager._instance.keymoveingpoint;
    }
    private void FixedUpdate()
    {
        target += new Vector3(0, amplitude * Mathf.Sin(speed * Time.fixedTime), 0);
        transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * 14f);
        // transform.RotateAround(transform.position, transform.up, 1);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            other.GetComponent<Player>().haskey = true;
            television.unlock();
            SoundManager._instance.unlockedsfx();
            gameObject.LeanMove(Collecting_position.position, 0.65f).setEaseLinear().setOnComplete(delegate () { Destroy(gameObject); });
        }

    }

}
