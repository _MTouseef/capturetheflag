﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform instantiation_point;
    public GameObject Bullet_prefab;
    Player player;
    Enemy enemy;
    public float shooting_time;
    float time;
    public Transform gun;
    public ParticleSystem muzzle;
    public bool isenemy;
    public bool canshoot;
    private void Start()
    {
        if (!isenemy)
        {
            player = GetComponent<Player>();
            enemy = GameObject.FindObjectOfType<Enemy>();
        }
        if (isenemy)
        {
            enemy = GetComponent<Enemy>();
            player = GameObject.FindObjectOfType<Player>();
        }
        time = shooting_time;
    }
    private void Update()
    {

        if (player && enemy)
        {
            if (player.withpistol && player.canshoot)
            {
                shoot();
                if (player.joystick.Direction == Vector2.zero && enemy && !isenemy)
                {
                    Vector3 direction = enemy.transform.position - transform.position;
                    Quaternion rotationtolook = Quaternion.LookRotation(direction);
                    transform.rotation = rotationtolook;
                }
            }
        }
        if (enemy && player)
        {
            if (enemy.withpistol && enemy.canshoot)
                shoot();
        }
    }
    void shoot()
    {
        if (!isenemy)
        {
            if (player.withpistol)
            {
                if (shooting_time <= 0)
                {
                    Instantiate(Bullet_prefab, instantiation_point.position, gun.rotation);
                    muzzle.Play();
                    Eventmanager._instance.Onshoot?.Invoke();
                    if (player.playerstate == Player.State.pistol_idle)
                    {
                        //  player.animator.SetTrigger("shoot");
                    }
                    shooting_time = time;
                }
                else
                    shooting_time -= Time.deltaTime;

            }
        }
        else if (isenemy)
        {
            if (enemy.withpistol)
            {
                if (shooting_time <= 0)
                {
                    Instantiate(Bullet_prefab, instantiation_point.position, gun.rotation);
                    muzzle.Play();
                    Eventmanager._instance.Onshoot?.Invoke();

                    if (enemy.playerstate == Enemy.State.idle)
                    {
                        //   player.animator.SetTrigger("shooting");
                    }
                    shooting_time = time;
                }
                else
                    shooting_time -= Time.deltaTime;

            }

        }
    }




}
