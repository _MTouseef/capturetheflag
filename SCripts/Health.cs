using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{

    public int health;
    public GameObject ragdoll;
    public bool isalive = true;
    public Transform healthrenderer;
    float totalhealth;
    private void Start()
    {
        totalhealth = health;
    }
    public void deducthealth()
    {
        if (health > 1)
        {
            health--;
            float ratio = health / totalhealth;
            healthrenderer.LeanScaleX(ratio, 0);
            Eventmanager._instance.OnDeductHealth?.Invoke();
        }
        else if (health == 1)
        {
            Instantiate(ragdoll, transform.position, transform.rotation);
            isalive = false;
            Eventmanager._instance.GameOver?.Invoke();
            Destroy(transform.gameObject);
        }
    }

}
