using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Television : MonoBehaviour
{
    public GameObject unlocked, locked;
    public void unlock()
    {
        unlocked.SetActive(true);
        locked.SetActive(false);
    }

}
