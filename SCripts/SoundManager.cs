using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    static public SoundManager _instance;
    private void Awake()
    {
        _instance = this;
    }
    private void Start()
    {
        Eventmanager._instance.Onshoot.AddListener(gunshot);
        Eventmanager._instance.GUnTimeOver.AddListener(recoil);
        Eventmanager._instance.Ongunpicked.AddListener(recoil);
        Eventmanager._instance.Ongunpicked.AddListener(starttimer);
        Eventmanager._instance.GameOver.AddListener(stoptimer);
    }
    public AudioSource sfxsource, footstepSource;
    public AudioClip[] sounds;
    public void gunshot()
    {
        sfxsource.PlayOneShot(sounds[0]);

    }

    public void recoil()
    {
        sfxsource.PlayOneShot(sounds[1]);
    }
    public void unlockedsfx()
    {
        sfxsource.PlayOneShot(sounds[4]);
    }

    int x = 1;
    public void footstep()
    {
        if (x == 1)
        {
            footstepSource.PlayOneShot(sounds[2]);
            x = 0;
        }
        else if (x == 0)
        {
            footstepSource.PlayOneShot(sounds[3]);
            x = 1;

        }
    }
    public AudioSource timersource;
    public void starttimer()
    {
        timersource.gameObject.SetActive(true);
    }
    public void stoptimer()
    {
        timersource.gameObject.SetActive(false);
    }

}
